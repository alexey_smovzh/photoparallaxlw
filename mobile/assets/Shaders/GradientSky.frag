/*
* fragment shader template
*/
uniform vec3 m_TopColor;
uniform vec3 m_BottomColor;
uniform vec2 m_Resolution;

#ifdef TEXTURE
    varying vec2 texCoord;

    uniform sampler2D m_Texture;
    uniform float m_Alpha;

#endif


void main() {

    vec2 position = gl_FragCoord.xy / m_Resolution.xy; 

    vec3 gradient = mix(m_BottomColor, m_TopColor, position.y);
    vec4 color = vec4(gradient, 1.0);


    #ifdef TEXTURE                                                              

        vec4 texColor = texture2D(m_Texture, texCoord);
        color = mix(vec4(gradient, 1.0), texColor, texColor.a * m_Alpha);

    #endif

    gl_FragColor = color;                                                       

}

