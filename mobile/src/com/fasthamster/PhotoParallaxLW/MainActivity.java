package com.fasthamster.PhotoParallaxLW;
 
import android.content.pm.ActivityInfo;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import com.jme3.app.AndroidHarness;
import com.jme3.system.android.AndroidConfigChooser.ConfigType;
import java.util.logging.Level;
import java.util.logging.LogManager;

import com.jme3.app.state.AppStateManager;
import com.jme3.math.Vector3f;


/*
 * http://hub.jmonkeyengine.org/t/using-the-accelerometer/19084
 */
 
public class MainActivity extends AndroidHarness implements SensorEventListener {
    
    private SensorManager sensorManager = null;
    private AppStateManager stateManager;

    
    private Vector3f initialPosition;
    
    /*
     * Note that you can ignore the errors displayed in this file,
     * the android project will build regardless.
     * Install the 'Android' plugin under Tools->Plugins->Available Plugins
     * to get error checks and code completion for the Android project files.
     */
 
    public MainActivity(){
        // Set the application class to run
        appClass = "PhotoParallaxLW.Main";
        // Try ConfigType.FASTEST; or ConfigType.LEGACY if you have problems
        eglConfigType = ConfigType.BEST;
        // Exit Dialog title & message
        exitDialogTitle = "Exit?";
        exitDialogMessage = "Press Yes";
        // Enable verbose logging
        eglConfigVerboseLogging = false;
        // Choose screen orientation
        screenOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;
        // Enable MouseEvents being generated from TouchEvents (default = true)
        mouseEventsEnabled = true;
        // Set the default logging level (default=Level.INFO, Level.ALL=All Debug Info)
        LogManager.getLogManager().getLogger("").setLevel(Level.INFO);
        
    }
    
    @Override
    protected void onResume() {
        
        super.onResume();
        
        // Register class as listener for accelerometer
        sensorManager.registerListener(this, 
                                       sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), 
                                       SensorManager.SENSOR_DELAY_NORMAL);
                
    }
    
    @Override
    protected void onStop() {
        
        sensorManager.unregisterListener(this);
        super.onStop();
        
    }
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        
        super.onCreate(savedInstanceState);
        
        sensorManager = (SensorManager)getSystemService(SENSOR_SERVICE);
        
    }
    
    @Override
    public void onDestroy() {
        
        super.onDestroy();
        System.runFinalization();
        android.os.Process.killProcess(android.os.Process.myPid());
        
    }
    
    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

        // TODO:
        
    }
    
/*
 * private Vector3 parralaxTranslation;
 * 
 * // обрабатываем изменение положения устройства, если есть акселерометр 
   if(handler.isAccelerometerAvailable() == true) {
        parralaxTranslation = handler.calcParralaxTranslation(devicePosition);
   }

   // анимируем модели в соотв. с параметрами сцены
   for(WallpaperObject f : instances) {				
																  // мльтипликатор в зависимости от удаленности объекта
        f.transform.translate(parralaxTranslation.x * ((SceneParams.FAR - f.depth) / 25f),
                              0,
                              parralaxTranslation.z * ((SceneParams.FAR - f.depth) / 15f));
   }
 * 
 * // Обрабатываем акселерометр, движения устройства
	public Vector3 calcParralaxTranslation(Vector2 devicePosition) {
		
		Vector3 parralaxTranslation = new Vector3(0, 0, 0);
		
		// X
		if(getAccelerometerX() - devicePosition.x > 0.5f) {			
			parralaxTranslation.x = SceneParams.PARRALAX_STEP;
			devicePosition.x += SceneParams.PARRALAX_STEP;
		}
		if(getAccelerometerX() - devicePosition.x < -0.5f) {
			parralaxTranslation.x = -SceneParams.PARRALAX_STEP;
			devicePosition.x -= SceneParams.PARRALAX_STEP;
		}
		
		// Z (перемещение телефона по оси Y, приближает/удаляет объекты, т.е. сдвигает по оси Z)
		if(getAccelerometerY() - devicePosition.y > 0.6f) {			
			parralaxTranslation.z -= SceneParams.PARRALAX_STEP;
			devicePosition.y += SceneParams.PARRALAX_STEP;
		}
		if(getAccelerometerY() - devicePosition.y < -0.6f) {
			parralaxTranslation.z = SceneParams.PARRALAX_STEP;
			devicePosition.y -= SceneParams.PARRALAX_STEP;
		}		
		
		return parralaxTranslation;
	}
 * 
 */    
    
    
    @Override
    public void onSensorChanged(SensorEvent event) {
        
        if(stateManager == null) {
            
            stateManager = getJmeApplication().getStateManager();
            
            return;         // wait for sensors
            
        }
        
        if(stateManager.getState(PhotoParallaxLW.PhotoParallaxState.class) == null) {
            
            return;         // wait for jme state
            
        }
        
        if(event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            
            // Save initial device position
            if(initialPosition == null) {
            
                initialPosition = new Vector3f(event.values[0], event.values[1], event.values[2]);
            
            }            

            
            stateManager.getState(PhotoParallaxLW.PhotoParallaxState.class).setRotation(
                                        new Vector3f(initialPosition.x - event.values[0],
                                                     initialPosition.y - event.values[1],
                                                     initialPosition.z - event.values[2]));
            
        }                    
    } 
}