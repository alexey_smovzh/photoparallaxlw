package PhotoParallaxLW;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */



import PhotoParallaxLW.ScenesHolder.SceneSet;
import com.jme3.app.Application;
import com.jme3.app.SimpleApplication;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import com.jme3.asset.AssetManager;
import com.jme3.asset.TextureKey;
import com.jme3.material.Material;
import com.jme3.material.RenderState;
import com.jme3.math.Vector3f;
import com.jme3.renderer.queue.RenderQueue;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.SceneGraphVisitor;
import com.jme3.scene.Spatial;
import com.jme3.texture.Texture;

/*
 * @author Alexey Smovzh <alexey.smovzh@gmail.com>
 *
 */

// todo: в ассертах сделать текстуры под разное разрешение
//       определять разрешение устройства и загружать соотв. текстуры

public class PhotoParallaxState extends AbstractAppState {
    
    private SimpleApplication app;
    private Node rootNode;
    private AssetManager assetManager;
    private AppStateManager stateManager;

    
    private Node scene;
    private TextureControl control;
    private Vector3f sceneRotation;    
    
    private BonfireState bonfire;
    private CloudState clouds;
    private AuroraState aurora;
    
    private ScenesHolder scenesHolder;                                          
    private SceneSet oldScene;                                                  // Old scene params
    public static boolean DAY = true;
    public static boolean NIGHT = false;    

    private static int SCENE_DURATION = 20;                                     // scene duration in seconds
    private static int INTERVAL = SCENE_DURATION * Main.FPS;                    // scene duration in frames
    
    
    
    // Visitor to setup textures and sets planes transparency                
    SceneGraphVisitor setupTextures = new SceneGraphVisitor() {

        public void visit(Spatial spatial) {
            if(spatial instanceof Geometry) {
                Geometry geo = (Geometry)spatial;
                
                Material mat = new Material(assetManager, "MatDefs/TextureColor.j3md");
                Texture texture = assetManager.loadTexture(new TextureKey("Blender/" + geo.getName() + ".png", true));                
                mat.setTexture("Texture", texture);
                mat.getAdditionalRenderState().setBlendMode(RenderState.BlendMode.Alpha);
                    
                geo.setMaterial(mat);
                geo.setQueueBucket(RenderQueue.Bucket.Translucent);                
                   
            }
        }            
    };
        
    // Load model at once
    private void loadModel() {
        
        scene = (Node)assetManager.loadModel("Blender/winter_mountain.j3o");
        scene.depthFirstTraversal(setupTextures);                        
        rootNode.attachChild(scene);   
        
    }
    
    // Load assets at once
    private void loadAssets() {
        
    } 
   
            
    // Controls
    public void setRotation(Vector3f rotation) {
        
        this.sceneRotation = rotation;
        
    }
    
    // turn day
    public void setDay() {

        if(oldScene.isDay == DAY) return;
        
        // Change texture color to day
        control.setSunsetDirection(TextureControl.NIGHT2DAY);
        scene.addControl(control);
            
        // Change sky color to day
        stateManager.getState(SkyState.class).setDay();

    }
    
    
    private static final byte NIGHT_WITHOUT_STARS = 0;
    private static final byte NIGHT_MILKYWAY = 1;
    private static final byte NIGHT_STARS = 2;
    
    // turn night
    public void setNight(byte type) {
        
        if(oldScene.isDay == NIGHT) return;
        
        // Change texture color to night
        control.setSunsetDirection(TextureControl.DAY2NIGHT);
        scene.addControl(control);
            
        // Change sky to night by type
        switch(type) {
            case NIGHT_WITHOUT_STARS: 
                stateManager.getState(SkyState.class).setNight();        
                break;
            case NIGHT_MILKYWAY: 
                stateManager.getState(SkyState.class).setNightWithMilkyway();                        
                break;
            case NIGHT_STARS: 
                stateManager.getState(SkyState.class).setNightWithStars();                        
                break;                
        }
    }

    // Change scene to new one
    private void changeScene() {
        
        SceneSet scene;
        
        // Load new scene params
        if(oldScene.isDay == DAY) {                                             // Current day - change to night            
            scene = scenesHolder.getRandomNightScene();                    
        } else {                                                                // Current night - change to day
            scene = scenesHolder.getRandomDayScene();            
        }
        
        // Detach old scene states
        if(oldScene.isCloud) stateManager.detach(clouds);
        if(oldScene.isBonfire) stateManager.detach(bonfire);
        if(oldScene.isAurora) stateManager.detach(aurora);
        
        // Setup new scene
        if(scene.isDay) setDay();
        if(scene.isMilkyWay) setNight(NIGHT_MILKYWAY);
        if(scene.isStars) setNight(NIGHT_STARS);        
        if(scene.isCloud) stateManager.attach(clouds);
        if(scene.isBonfire) stateManager.attach(bonfire);
        if(scene.isAurora) stateManager.attach(aurora);        
        
        oldScene = scene;              
        
    }
    
    
    int i = 0;    
    @Override
    public void update(float tpf) {
                
/*        scene.setLocalRotation(new Quaternion().fromAngles(FastMath.DEG_TO_RAD * -sceneRotation.y,
                                                           FastMath.DEG_TO_RAD * sceneRotation.x,
                                                           0f));
        
        // todo: приближение/удаление
        scene.setLocalTranslation(0f, 0f, -sceneRotation.y * 0.1f);
  */      
        //
        
        // Scene changer
        i++;     
        if(i == INTERVAL) {
         
            changeScene();
            i = 0;
            
        }               
    }

    @Override
    public void initialize(AppStateManager stateManager, Application app) {
        
        super.initialize(stateManager, app);
        
        this.app = (SimpleApplication)app;
        this.stateManager = this.app.getStateManager();        
        this.assetManager = this.app.getAssetManager();  
        this.rootNode = this.app.getRootNode(); 

        loadAssets();                
        loadModel(); 
        
        control = new TextureControl(this.stateManager);
        bonfire = new BonfireState(scene, 5);          // todo: profable memory leak, need to discover
        clouds = new CloudState();
        aurora = new AuroraState();
        
        scenesHolder = new ScenesHolder();
        oldScene = scenesHolder.getDummyScene();                                // first run, load dummy oldScene
                                                                                // doing this we avoid a lot if-else statemets
    }        
    
    @Override
    public void cleanup() {
        
        super.cleanup();
        
        this.app.getRootNode().detachAllChildren();
        
    }

}
