/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package PhotoParallaxLW;

import com.jme3.math.FastMath;

/*
 * @author Alexey Smovzh <alexey.smovzh@gmail.com>
 *
 */


/*
* Scenes list:
* 
* day - clear
* day - cloud1
* day - cloud2
* night - milkyway
* night - milkyway + bonfire
* night - stars + aurora1
* night - stars + aurora2
* 
* for test:
* night - stars
* night - milkyway + clouds
* day - bonfire
* 
*/

public class ScenesHolder {

    // Scene description
    static class SceneSet {
        
        boolean isDay;
        boolean isCloud;
        boolean isBonfire;
        boolean isAurora;
        boolean isMilkyWay;
        boolean isStars;
                
        public SceneSet(boolean isDay, boolean isCloud, boolean isBonfire, 
                        boolean isAurora, boolean isMilkyWay, boolean isStars) {
            
            this.isDay = isDay;
            this.isCloud = isCloud;
            this.isBonfire = isBonfire;            
            this.isAurora = isAurora;
            this.isMilkyWay = isMilkyWay;
            
            if(isMilkyWay == true && isStars == true) {
                throw new IllegalArgumentException("Milky way texture and shader stars can't be in the same scene.");
            } else {
                this.isStars = isStars;
            }
            
        }
    }
    
    private static final SceneSet[] dayScenes = new SceneSet[] {
        
        new SceneSet(true, false, false, false, false, false),                  // day clear
        new SceneSet(true, true, false, false, false, false)                    // day with clouds (cloud type randomly selected by CloudState.class)
        
    };

    private static final SceneSet[] nightScenes = new SceneSet[] {
        
        new SceneSet(false, false, false, false, true, false),                  // night + milkyway
        new SceneSet(false, false, true, false, true, false),                   // night + milkyway + bonfire
        new SceneSet(false, false, false, true, false, true)                    // night + stars + aurora (aurora type randomly selected by AuroraState.class)
        
    };
    
    // Getters
    public SceneSet getDummyScene() {                                           // Dummy scene for oldScene first app run
        
        return new SceneSet(true, false, false, false, false, false);
                
    }
    
    public SceneSet getRandomDayScene() {
    
        int idx = FastMath.rand.nextInt(dayScenes.length);
        
        return dayScenes[idx];
        
    }
    
    public SceneSet getRandomNightScene() {
    
        int idx = FastMath.rand.nextInt(nightScenes.length);
        
        return nightScenes[idx];
        
    }
}

 
    