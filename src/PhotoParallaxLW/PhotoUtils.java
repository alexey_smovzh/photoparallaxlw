/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package PhotoParallaxLW;

import com.jme3.system.JmeSystem;
import com.jme3.texture.Image;
import com.jme3.texture.Texture;
import com.jme3.util.BufferUtils;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.zip.DataFormatException;
import java.util.zip.Deflater;
import java.util.zip.Inflater;

/*
 * @author Alexey Smovzh <alexey.smovzh@gmail.com>
 *
 */

public class PhotoUtils {

    // Constructor
    public PhotoUtils() {
        
    }
    
    // Convert ARGB to RGBA
    private ByteBuffer argb2rgba(ByteBuffer source) {
        
        ByteBuffer buffer = BufferUtils.clone(source);
        
        for(int i = 0; i < buffer.capacity(); i += 4) {
   
                byte a = buffer.get(i);
                byte r = buffer.get(i + 1);
                byte g = buffer.get(i + 2);
                byte b = buffer.get(i + 3);
                
                buffer.put(i, (byte)r);               // r
                buffer.put(i + 1, (byte)g);           // g
                buffer.put(i + 2, (byte)b);           // b
                buffer.put(i + 3, (byte)a);           // a
                
        }        
        
        return buffer;
    }
    
    // Save texture to png
    public void savePNG(Texture texture, String path) {
        
        Image image = texture.getImage();
        
        FileOutputStream stream = null;
        try {
            File file = new File(path).getAbsoluteFile();
            stream = new FileOutputStream(file);
            
            JmeSystem.writeImageFile(stream,  "png", argb2rgba(image.getData(0)), image.getWidth(), image.getHeight());                        

        } catch (IOException e) {
            System.out.println(e);
        } finally {
            if(stream != null) {                
                try {
                    stream.flush();
                    stream.close();                    
                } catch (IOException e) {
                    System.out.println(e);
                }    
            }
        }        
    }
    
    // Save mask byte array to file
    private void save(byte[] image, String path) {
        
        FileOutputStream stream = null;
        try {
            File file = new File(path).getAbsoluteFile();
            stream = new FileOutputStream(file);
            
            stream.write(image);
                        
        } catch (IOException e) {
            System.out.println(e);
        } finally {
            if(stream != null) {                
                try {
                    stream.flush();
                    stream.close();                    
                } catch (IOException e) {
                    System.out.println(e);
                }    
            }
        }        
    }
    
    // Compress http://qupera.blogspot.com/2013/02/howto-compress-and-uncompress-java-byte.html
    public byte[] compress(byte[] data) throws IOException {
        
        Deflater deflater = new Deflater();
            
        deflater.setInput(data);
        ByteArrayOutputStream stream = new ByteArrayOutputStream();    
        deflater.finish();
        
        byte[] buffer = new byte[1024];
        
        while(!deflater.finished()) {
            int count = deflater.deflate(buffer);
            stream.write(buffer, 0, count);
        }
        
        stream.close();
        
        byte[] result = stream.toByteArray();
        
        deflater.end();
        
        return result;               
    }
    
    // Decompress
    public byte[] decompress(byte[] data) throws DataFormatException, IOException {
        
        Inflater inflater = new Inflater();
        inflater.setInput(data);
        
        ByteArrayOutputStream stream = new ByteArrayOutputStream(data.length);
        
        byte[] buffer = new byte[1024];
        
        while(!inflater.finished()) {
            int count = inflater.inflate(buffer);
            stream.write(buffer, 0, count);
        }
        
        stream.close();
        
        byte[] result = stream.toByteArray();
        
        inflater.end();
        
        return result;
    }    
    
    
}
