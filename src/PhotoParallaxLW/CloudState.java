/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package PhotoParallaxLW;

import com.jme3.app.Application;
import com.jme3.app.SimpleApplication;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import com.jme3.asset.AssetManager;
import com.jme3.effect.ParticleEmitter;
import com.jme3.effect.ParticleMesh;
import com.jme3.effect.shapes.EmitterBoxShape;
import com.jme3.material.Material;
import com.jme3.material.RenderState;
import com.jme3.math.ColorRGBA;
import com.jme3.math.FastMath;
import com.jme3.math.Vector3f;
import com.jme3.scene.Node;

/*
 * @author Alexey Smovzh <alexey.smovzh@gmail.com>
 *
 */

public class CloudState extends AbstractAppState{
      
    private SimpleApplication app;
    private Node rootNode;
    private AssetManager assetManager;    
    private ParticleEmitter cloud;   
    
    // Environment params
    private Vector3f firstCorner;                                               // corners for emitter box shape
    private Vector3f secondCorner;
    private float speed;                                                        // clouds speed and direction
    private float speedVariantion;
    private CloudParams params;
    
    private ColorRGBA color = DEFAULT_COLOR;                                    // day/night color
    private static final ColorRGBA DEFAULT_COLOR = new ColorRGBA(1f, 1f, 1f, .9f);
    
    private static final boolean WIND_RIGHT_2_LEFT = true;                      // wind directions
    private static final boolean WIND_LEFT_2_RIGHT = false;    
        
    private static final float SPEED = .0015f;
    private static final float SPEED_VARIANTION = .001f;
    

    // Cloud params holder class
    private static class CloudParams {

        private String texturePath;
        private int pX, pY;                                                         // texture size
        private int count;                                                          // particles count
        private float startSize;
        private float endSize;
        private float rotateSpeed;
        private float particlesPerSec;
        private boolean quadratic;
        
        public CloudParams(String texturePath, int pX, int pY, int count, 
                     float startSize, float endSize, float rotateSpeed, 
                     float particlesPerSec, boolean quadratic) {
            
            this.texturePath = texturePath;
            this.pX = pX;
            this.pY = pY;
            this.count = count;
            this.startSize = startSize;
            this.endSize = endSize;
            this.rotateSpeed = rotateSpeed;
            this.particlesPerSec = particlesPerSec;
            this.quadratic = quadratic;
            
        }
        
        // Cloud params
        private static final CloudParams[] cloudParam = new CloudParams[] {
                                                          // size -.1f
            new CloudParams("Blender/cloud0.png", 4, 4, 40, .2f, .2f, .05f, .6f, true),     // Cloudy
            new CloudParams("Blender/cloud1.png", 4, 4, 10, .3f, .3f, 0f, .05f, false)      // Cumulus cloud
        };
        
        // Getter
        public static CloudParams getCloudParams() {
            
            return cloudParam[FastMath.rand.nextInt(cloudParam.length)];
            
        }
    }    
    
    // Sets emitter box shape cornes based on wind direction
    private void setupParams() {
        
        if(FastMath.rand.nextBoolean() == WIND_RIGHT_2_LEFT) {
            firstCorner = new Vector3f(1f, .7f, -1.2f);
            secondCorner = new Vector3f(4f, 1.2f, 0f);
            
            speed = SPEED;
            speedVariantion = SPEED_VARIANTION;
            
        } else {
            firstCorner = new Vector3f(-1f, .7f, -1.2f);
            secondCorner = new Vector3f(-4f, 1.2f, 0f);            
            
            speed = -SPEED;
            speedVariantion = -SPEED_VARIANTION;
            
        }        
        
        params = CloudParams.getCloudParams();
        
    }
     
    // Setup clouds particle emitters
    private void setupClouds() {
                
        Material material = new Material(assetManager, "Common/MatDefs/Misc/Particle.j3md");
        material.setTexture("Texture", assetManager.loadTexture(params.texturePath));
        material.getAdditionalRenderState().setBlendMode(RenderState.BlendMode.Color);
        material.setFloat("Softness", 3f); // 
        
        if(params.quadratic) material.setFloat("Quadratic", 4f); 
        
        cloud = new ParticleEmitter("Cloud", ParticleMesh.Type.Triangle, params.count);
        cloud.setMaterial(material);
        cloud.setShape(new EmitterBoxShape(firstCorner, secondCorner));
        cloud.setImagesX(params.pX);
        cloud.setImagesY(params.pY); 
        
        cloud.setSelectRandomImage(true);
        cloud.getParticleInfluencer().setVelocityVariation(speedVariantion);        

        if(params.rotateSpeed != 0) {
            cloud.setRotateSpeed(params.rotateSpeed);
            cloud.setRandomAngle(true);
        }

        cloud.setStartColor(color);
        cloud.setEndColor(color);
        
        cloud.setStartSize(params.startSize);
        cloud.setEndSize(params.endSize);
        cloud.setGravity(speed, 0f, 0f);
        cloud.setLowLife(80f);
        cloud.setHighLife(80f);
        cloud.setParticlesPerSec(params.particlesPerSec);
        
        rootNode.attachChild(cloud);
       
    }
    
    // Setters
    public void setColor(ColorRGBA color) {
        
        ColorRGBA result = color.add(new ColorRGBA(0f, 0f, 0f, 1f));        
        
        this.color = result;
        cloud.setStartColor(result);
        cloud.setEndColor(result);        
        
    }
    
    
    @Override
    public void cleanup() {
        
        super.cleanup();
        
        rootNode.detachChild(cloud);
        
    }
    
    @Override
    public void initialize(AppStateManager stateManager, Application app) {
        
        super.initialize(stateManager, app);
        
        this.app = (SimpleApplication)app;
        this.rootNode = this.app.getRootNode();
        this.assetManager = this.app.getAssetManager();
        
        // Setup parameters
        setupParams();
        
        // Start clouding
        setupClouds();
        
    }
}
