/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package PhotoParallaxLW;

import com.jme3.app.Application;
import com.jme3.app.SimpleApplication;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import com.jme3.asset.AssetManager;
import com.jme3.asset.TextureKey;
import com.jme3.effect.ParticleEmitter;
import com.jme3.effect.ParticleMesh;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector3f;
import com.jme3.renderer.queue.RenderQueue;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.texture.Texture;

/*
 * @author Alexey Smovzh <alexey.smovzh@gmail.com>
 *
 */

public class BonfireState extends AbstractAppState {
    
    private SimpleApplication app;
    private Node rootNode;
    private AssetManager assetManager;
    
    private Node scene;
    private int delay;                                                          // delay to bonfire on in seconds
    private Texture texture;
    private Geometry geometry;
    ParticleEmitter fireEmitter;
    
    private static final String PLANE = "Plane2";
    private static final Vector3f POSITION = new Vector3f(-0.27f, -0.45f, 0.15f);
    
    
    // Constructor
    // Node scene - to change plane2 texture to texture plane2_nf (with bonfire light)
    // int delay - delay in seconds to on bonfire
    public BonfireState(Node scene, int delay) {
        
        this.scene = scene;
        this.delay = delay * Main.FPS;
        
    }
    
    // Setup particle emitter
    private void setupBonfire() {
        
        fireEmitter = new ParticleEmitter("fire emitter", ParticleMesh.Type.Triangle, 30);
        
        Material fireMat = new Material(assetManager, "Common/MatDefs/Misc/Particle.j3md");
        fireMat.setTexture("Texture", assetManager.loadTexture("Blender/flame.png"));
        fireEmitter.setMaterial(fireMat);
        fireEmitter.setImagesX(4);
        fireEmitter.setImagesY(4);
        fireEmitter.setSelectRandomImage(true);
//        fireEmitter.setRandomAngle(true);
        fireEmitter.setStartColor(new ColorRGBA(.5f, .5f, .5f, 0.8f));
        fireEmitter.setEndColor(new ColorRGBA(0f, 0f, 0f, 0f));
        fireEmitter.setGravity(0f, 0f, 0f);                
        fireEmitter.getParticleInfluencer().setInitialVelocity(new Vector3f(0f, 0.15f, 0f));
        fireEmitter.getParticleInfluencer().setVelocityVariation(.2f);
        fireEmitter.setStartSize(0.08f);
        fireEmitter.setEndSize(.002f);
        fireEmitter.setLowLife(.5f);
        fireEmitter.setHighLife(2f);
        
        fireEmitter.setQueueBucket(RenderQueue.Bucket.Translucent);   
       
        fireEmitter.setLocalTranslation(POSITION);
        
        rootNode.attachChild(fireEmitter);

    }
    
    // Load assets at once
    private void loadAssets() {
        
        texture = assetManager.loadTexture(new TextureKey("Blender/Plane2_nf.png", true));                
        
    }
    
    // Change plane2 texture image to Plane2_nf.png
    private void setupBonfireLightTexture() {
        
        Node plane = (Node)scene.getChild(PLANE);
        geometry = (Geometry)plane.getChild(0);
        geometry.getMaterial().setTexture("BonfireLight", texture);                                                            

    }
    
    // Clear BonfireLight material param
    private void clearBonfireLightTexture() {

        geometry.getMaterial().clearParam("BonfireLight");

    }
    
    int i = 0;
    @Override
    public void update(float tpf) {
        
        if(i == delay) {
            
            setupBonfireLightTexture();
            setupBonfire();
            
        }
        i++;
    }   
    
    @Override
    public void cleanup() {
        
        super.cleanup();
        
        clearBonfireLightTexture();
        
        rootNode.detachChild(fireEmitter);
        
    }
 
    @Override
    public void initialize(AppStateManager stateManager, Application app) {
        
        super.initialize(stateManager, app);
        
        this.app = (SimpleApplication)app;
        this.assetManager = this.app.getAssetManager();  
        this.rootNode = this.app.getRootNode(); 
        
        loadAssets();       
        
    }        
}
