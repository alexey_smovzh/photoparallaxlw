/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package PhotoParallaxLW;

import com.jme3.app.state.AppStateManager;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector3f;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.SceneGraphVisitor;
import com.jme3.scene.Spatial;
import com.jme3.scene.control.AbstractControl;

/*
 * @author Alexey Smovzh <alexey.smovzh@gmail.com>
 *
 */

public class TextureControl extends AbstractControl {
    
    private AppStateManager stateManager;
    private Node scene;
    private int currentStep;
    private int increment;
    
    private static final int SUNSET_DURATION = 1;                               // 1 second
    private static final int SUNSET_STEPS = Main.FPS * SUNSET_DURATION;         // steps for update cycle
    

    private boolean day2night;
    public static boolean DAY2NIGHT = true;
    public static boolean NIGHT2DAY = false;

    
    private ColorRGBA color;
    
    
    // Constructor
    public TextureControl(AppStateManager stateManager) {
        
        this.stateManager = stateManager;
        
    }
    
    // Visitor to apply mask for textures
    SceneGraphVisitor changeTimeOfDay = new SceneGraphVisitor() {
            
        public void visit(Spatial spatial) {             
            if(spatial instanceof Geometry) {                    
                Geometry geo = (Geometry)spatial;                    
                geo.getMaterial().setColor("Color", color);
            }                
        }            
    };    
    
    // Visitor to clear Color material param
    SceneGraphVisitor clearColorParam = new SceneGraphVisitor() {
        
        public void visit(Spatial spatial) {             
            if(spatial instanceof Geometry) {                    
                Geometry geo = (Geometry)spatial;                    
                geo.getMaterial().clearParam("Color");
            }                
        }              
    };
  
    
    @Override
    public void setSpatial(Spatial spatial) {
        
        super.setSpatial(spatial);
        
        if(spatial != null) scene = (Node)spatial;
        
    }    
    
    private static final Vector3f min = new Vector3f(.92f, .93f, .97f);                        
    private static final Vector3f mask = new Vector3f(.84f, .77f, .67f);
    // Get color for TextureColor shader based on currentStep
    private ColorRGBA getColor(int step) {
        
        Vector3f result = min.subtract(new Vector3f((mask.x / SUNSET_STEPS) * step,
                                                    (mask.y / SUNSET_STEPS) * step,
                                                    (mask.z / SUNSET_STEPS) * step));
        
        return new ColorRGBA(result.x, result.y, result.z, 0f);
        
    }
    
    // Set texture color change direction, day to night or night to day
    public void setSunsetDirection(boolean direction) {
        
        this.day2night = direction;
                
        if(day2night == DAY2NIGHT) {             // add              i+=increment
            increment = 1;
            currentStep = 1;
        } else {                                 // substract        i+=-increment
            increment = -1;
            currentStep = SUNSET_STEPS;
        }        
    }

    
    @Override
    protected void controlUpdate(float tpf) {
        
        // Get color for shader 
        color = getColor(currentStep);
        
        // Apply color to geometrys
        scene.depthFirstTraversal(changeTimeOfDay);
        
        // Change step
        currentStep += increment;

        // amination end, detach control
        if(currentStep == SUNSET_STEPS) spatial.removeControl(this);
        
        // amination end, detach control and clear Color material param
        if(currentStep == 0) {
            
            scene.depthFirstTraversal(clearColorParam);
            spatial.removeControl(this);
        }
        
    }
    

    @Override
    protected void controlRender(RenderManager rm, ViewPort vp) {   }
        

}

