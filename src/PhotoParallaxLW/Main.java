package PhotoParallaxLW;

import com.jme3.app.SimpleApplication;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.jme3.renderer.RenderManager;
import com.jme3.system.AppSettings;



/*
 * @author Alexey Smovzh <alexey.smovzh@gmail.com>
 *
 */


public class Main extends SimpleApplication {  
    
    public static final int FPS = 20;                                           // Limit game fps
    
    
    @Override
    public void simpleInitApp() {
                
        setupCam();
        
        
        PhotoParallaxState state = new PhotoParallaxState();
        stateManager.attach(state);
        
        SkyState sky = new SkyState();
        stateManager.attach(sky);
        
    }   
    
    // Sleep for limit fps on Android, setFrameRate on Android don't work
    private long diff, start = System.currentTimeMillis();	
    private void sleep(int fps) {
		
        diff = System.currentTimeMillis() - start;
        long targetDelay = 1000 / fps;
        
        if(diff < targetDelay) {
            try {
                Thread.sleep(targetDelay - diff);
            } catch (InterruptedException e) {	}
        }
        
        start = System.currentTimeMillis();
    }
    
    @Override
    public void simpleUpdate(float tpf) {
        
        sleep(FPS);
        
    }

    @Override
    public void simpleRender(RenderManager rm) {
        
//        rm.setAlphaToCoverage(true);
        
    }
    
    private void setupCam() {

        flyCam.setMoveSpeed(20f);
        
        cam.setLocation(new Vector3f(0f, .4f, 3f));        
        cam.setRotation(new Quaternion(2.2409371E-5f, 0.9997362f, 0.022947192f, -9.7630476E-4f));
        
    }
    
    public static void main(String[] args) {
         
        Main app = new Main();
                       
        AppSettings settings = new AppSettings(true);
//        settings.setResolution(540, 960);                   // original 540, 960
        settings.setSamples(2);
        
//        settings.setFrameRate(FPS);
        
        app.setSettings(settings);
        app.setShowSettings(false);
        
        app.start();

     }
}
