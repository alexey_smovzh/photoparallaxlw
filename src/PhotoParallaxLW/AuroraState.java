/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package PhotoParallaxLW;

import com.jme3.app.Application;
import com.jme3.app.SimpleApplication;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import com.jme3.asset.AssetManager;
import com.jme3.asset.TextureKey;
import com.jme3.material.Material;
import com.jme3.material.RenderState;
import com.jme3.math.FastMath;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector2f;
import com.jme3.math.Vector3f;
import com.jme3.renderer.queue.RenderQueue;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.VertexBuffer;
import com.jme3.util.BufferUtils;
import java.nio.FloatBuffer;

/*
 * @author Alexey Smovzh <alexey.smovzh@gmail.com>
 *
 */

public class AuroraState extends AbstractAppState{
      
    private SimpleApplication app;
    private Node rootNode;
    private AssetManager assetManager;  

    private Node aurora;
    private Geometry plane;
    private boolean angleDir;                                                   // direction to rotate plane clockwise/conter
    
    private int height;
    private int width;

    private AuroraParams params;    
    private ColorsHolder colorsHolder;
    private float Speed;
    private float Wave;
    
    private static final boolean CLOCKWISE = true;
    private static final boolean CONTERCLOCKWISE = false;
    private static final int TEXTURE_COUNT_U = 4;  
    private static final int TEXTURE_COUNT_V = 2;  
    private static final Vector3f POSITION = new Vector3f(-1.5f, 2f, -1.5f);
    private static final int ANGLE = 5;                                         // max plane angle in deg

    
    
    // Aurora params holder
    private static class AuroraParams {
        
        private String texture;
        private int num;
        private int xOffset;
        private float yOffset;
        private float zOffset;
        private float scale;
        
        public AuroraParams(String texture, int num, int xOffset, float yOffset, float zOffset, float scale) {
            
            this.texture = texture;
            this.num = num;
            this.xOffset = xOffset;
            this.yOffset = yOffset;
            this.zOffset = zOffset;
            this.scale = scale;
            
        }
        
        // Aurora params
        private static final AuroraParams[] auroraParam = new AuroraParams[] {
            new AuroraParams("Blender/aurora0.png", 4, 20, .2f, .4f, .2f),      // -.1
            new AuroraParams("Blender/aurora0.png", 3, 40, -.1f, .5f, .3f),     // -.1
            new AuroraParams("Blender/aurora1.png", 12, 40, 0f, .4f, .1f),      // -.05
            new AuroraParams("Blender/aurora1.png", 6, 40, .2f, .4f, .2f)       // -.1     
        };        
        
        // Getters
        public static AuroraParams getAuroraParams() {
            
            return auroraParam[FastMath.rand.nextInt(auroraParam.length)];
                    
        }
    }
    
    // Setup auroras particle emitters
    private void setupAurora() {

        aurora = new Node("Aurora");
                
        // Set direction to rotate plane
        angleDir = FastMath.rand.nextBoolean();
        
        // Set wave speed and amplitude
        Wave = .2f + (0.4f - .2f) * FastMath.rand.nextFloat();
        Speed = .2f + (0.4f - .2f) * FastMath.rand.nextFloat();
        
        // Load params
        params = AuroraParams.getAuroraParams();
        
        Node m1 = (Node)assetManager.loadModel("Blender/aurora.j3o");
        Node m2 = (Node)m1.getChild("Plane1");
        plane = (Geometry)m2.getChild("Plane1");
        
        for(int i = 0; i < params.num; i++) {            
            emit((Geometry)plane.deepClone(), (float)i * params.yOffset, -(float)i * params.zOffset);
        }        
        
        aurora.setLocalTranslation(POSITION);        
        rootNode.attachChild(aurora);
       
    }
    
    // Change uv mapping for multicolumn texture
    private void setUVmappingMult(Geometry model, int num) {
        
        int row, column;
        
        if(num >= TEXTURE_COUNT_U) {
            row = num - TEXTURE_COUNT_U;
            column = num / TEXTURE_COUNT_U;
        } else {
            row = num;
            column = 0;
        }
        
        float offsetRow = 1f / TEXTURE_COUNT_U * row;
        float offsetCol = 1f / TEXTURE_COUNT_V * column;
        
        VertexBuffer uv = model.getMesh().getBuffer(VertexBuffer.Type.TexCoord);
        float[] uvArray = BufferUtils.getFloatArray((FloatBuffer)uv.getData());
        
        for(int i = 0; i < uvArray.length; i += 2) {
            
            float u = uvArray[i+1] / TEXTURE_COUNT_U;
            float v = uvArray[i] / TEXTURE_COUNT_V;
            uvArray[i+1] = u + offsetRow;
            uvArray[i] = v + offsetCol;            

        }        
        
        uv.updateData(BufferUtils.createFloatBuffer(uvArray));
    }
    
    // Return random float angle in range with random positive/negative value
    // only Y and Z axis
    private Vector3f randomAngle(){

        if(angleDir == CLOCKWISE) {
            return new Vector3f(0f, (float)FastMath.rand.nextInt(ANGLE), (float)FastMath.rand.nextInt(ANGLE));

        } else {
            return new Vector3f(0f, -(float)FastMath.rand.nextInt(ANGLE), -(float)FastMath.rand.nextInt(ANGLE));    
        }
    }

    // Emit aurora geometrics
    private void emit(Geometry model, float y, float z) {

        // Scale
        model.scale(params.scale);
        
        // Randomly rotate aurora plane in Y, Z axis        
        Vector3f vector = randomAngle();
        model.setLocalRotation(new Quaternion().fromAngles(-FastMath.PI / 2, 
                                                           FastMath.DEG_TO_RAD * vector.y, 
                                                           FastMath.DEG_TO_RAD * vector.z));

        // Randomly change plane X position
        float x = (float)FastMath.rand.nextInt(params.xOffset) / 10;
        
        model.setLocalTranslation(new Vector3f(x, y, z));

        // Getting colors
        ColorsHolder.ColorsSet colors = colorsHolder.getRandomAuroraColors();
        
        
        // Set random texture by UV coordinates
        // setUVmappingSingle(model, FastMath.rand.nextInt(TEXTURE_COUNT_X));
        setUVmappingMult(model, FastMath.rand.nextInt(TEXTURE_COUNT_U * TEXTURE_COUNT_V));        
        
                
        Material material = new Material(assetManager, "MatDefs/AuroraVertex.j3md");
        material.setTexture("ColorMap", assetManager.loadTexture(new TextureKey(params.texture, false)));
        material.setVector2("Resolution", new Vector2f(width, height));
        material.setVector3("Color0", colors.Color0);
        material.setVector3("Color1", colors.Color1);        
        material.setFloat("Wave", Wave);
        material.setFloat("Speed", Speed);

        material.getAdditionalRenderState().setFaceCullMode(RenderState.FaceCullMode.Off);
        material.getAdditionalRenderState().setBlendMode(RenderState.BlendMode.Color);
        model.setQueueBucket(RenderQueue.Bucket.Translucent);                         

        model.setMaterial(material);

        aurora.attachChild(model);

    }
    
    
    @Override
    public void cleanup() {
        
        super.cleanup();
        
        rootNode.detachChild(aurora);
        
    }
    
    @Override
    public void initialize(AppStateManager stateManager, Application app) {
        
        super.initialize(stateManager, app);
        
        this.app = (SimpleApplication)app;
        this.rootNode = this.app.getRootNode();
        this.assetManager = this.app.getAssetManager();
        
        this.width = this.app.getContext().getSettings().getWidth();
        this.height = this.app.getContext().getSettings().getHeight();
        
        colorsHolder = new ColorsHolder();
        
        // Setup aurora
        setupAurora();
        
    }
}
