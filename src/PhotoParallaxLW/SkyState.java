/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package PhotoParallaxLW;

import PhotoParallaxLW.ColorsHolder.ColorsSet;
import com.jme3.app.Application;
import com.jme3.app.SimpleApplication;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import com.jme3.asset.AssetManager;
import com.jme3.material.Material;
import com.jme3.material.RenderState;
import com.jme3.math.FastMath;
import com.jme3.math.Matrix3f;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector2f;
import com.jme3.math.Vector3f;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import com.jme3.renderer.queue.RenderQueue;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.control.AbstractControl;
import com.jme3.scene.shape.Sphere;

/*
 * @author Alexey Smovzh <alexey.smovzh@gmail.com>
 *
 */

public class SkyState extends AbstractAppState {
    
    
    private SimpleApplication app;
    private Node rootNode;
    private AssetManager assetManager;    

    private Geometry sky;
    private int width, height;
    private ColorsHolder colorsHolder;
    
    private Vector3f topColor, bottomColor;
    private float starsAlpha;
    
    private static final float RADIUS = 10f;
    private static final float SPEED = .01f;
    private static final String MILKYWAY_TEXTURE = "Blender/starfield.png";
    private static final String STARS_TEXTURE = "Blender/stars.png";
    
    
    
    // Setup sky
    private void setupSky() {
        
        sky = new Geometry("Background", new Sphere(16, 16, RADIUS));
        
        Material material = new Material(assetManager, "MatDefs/GradientSky.j3md");
        material.setVector3("TopColor", topColor);        
        material.setVector3("BottomColor", bottomColor);
        material.setVector2("Resolution", new Vector2f(width, height));
        material.getAdditionalRenderState().setFaceCullMode(RenderState.FaceCullMode.Front);
        sky.setMaterial(material);     
         
        sky.setQueueBucket(RenderQueue.Bucket.Sky);
        
        rootNode.attachChild(sky);
   
    }

    // Control for stars sphere rotation
    private class StarsTextureRotationControl extends AbstractControl {

        @Override
        protected void controlUpdate(float tpf) {
            
            spatial.rotate(new Quaternion().fromAngleAxis(SPEED * tpf, Vector3f.UNIT_Z));
            
        }

        @Override
        protected void controlRender(RenderManager rm, ViewPort vp) {  }
        
    }    
    
    // Change sky color
    private class SkyColorControl extends AbstractControl {
        
        private ColorsSet colors;
        private int step = 0;
        
        public SkyColorControl(ColorsSet colors) {
            
            this.colors = colors;
            
        }

        @Override
        protected void controlUpdate(float tpf) {

            // Change sky color
            topColor.interpolate(colors.Color0, step * tpf);
            bottomColor.interpolate(colors.Color1, step * tpf);
            
            if(step == Main.FPS) spatial.removeControl(this);

            step++;
        }

        @Override
        protected void controlRender(RenderManager rm, ViewPort vp) {  }        
    }
    
    // Control for change stars texture alpha
    private class StarsControl extends AbstractControl {
        
        private int step = 0;        
        private float increment;
        
        
        public StarsControl(float level, String texture) {
            // positive increment to add alpha from 0f to 1f            
            // negative increment to low alpha from 1f to 0f             
            increment = level == 1 ? 1f / Main.FPS : -1f / Main.FPS;
            
            if(texture != null) setupTexture(texture);

        }
        
        // Control methods
        private void setupTexture(String texture) {
            
            sky.getMaterial().setTexture("Texture", assetManager.loadTexture(texture));
            sky.getMaterial().setFloat("Alpha", starsAlpha);
            
            // Random X angle
            sky.setLocalRotation(new Quaternion().fromAngles(FastMath.DEG_TO_RAD * (FastMath.rand.nextInt()), 
                                                             FastMath.DEG_TO_RAD * 90, 0f));

            sky.addControl(new StarsTextureRotationControl());
            
        }
                
        private void nightTextureOff() { 
            
            sky.getMaterial().clearParam("Texture"); 
            sky.removeControl(StarsTextureRotationControl.class);
            
        }
        
        @Override
        protected void controlUpdate(float tpf) {    
           
            starsAlpha += increment;
  
            if(starsAlpha < 0f) starsAlpha = 0f;
            if(starsAlpha > 1f) starsAlpha = 1f;
            

            sky.getMaterial().setFloat("Alpha", starsAlpha);
            
            // Stars texture now fully transparent, so unload it
            if(starsAlpha == 0)  nightTextureOff();
            
            // Control finish its job, remove it
            if(step == Main.FPS) sky.removeControl(this);

            step++;

        }

        @Override
        protected void controlRender(RenderManager rm, ViewPort vp) {  }

    }
    
    
    // Controls
    public void setDay() {
        
        sky.addControl(new SkyColorControl(colorsHolder.getRandomDayColors()));

        // turn off stars        
        sky.addControl(new StarsControl(0f, null));

    }
    
    public void setNight() {
        
        sky.addControl(new SkyColorControl(colorsHolder.getRandomNightColors()));
        
    }
    
    public void setNightWithMilkyway() {
        
        sky.addControl(new SkyColorControl(colorsHolder.getRandomNightColors()));
        sky.addControl(new StarsControl(1f, MILKYWAY_TEXTURE));
        
    }
    
    public void setNightWithStars() {
        
        sky.addControl(new SkyColorControl(colorsHolder.getRandomNightColors()));
        sky.addControl(new StarsControl(1f, STARS_TEXTURE));
        
    }

    
    // Load models at once
    private void loadModels() {
        
        setupSky();
        
    }
    
    
    @Override
    public void initialize(AppStateManager stateManager, Application app) {
        
        super.initialize(stateManager, app);
        
        this.app = (SimpleApplication)app;
        this.rootNode = this.app.getRootNode();
        this.assetManager = this.app.getAssetManager();

        this.width = this.app.getContext().getSettings().getWidth();
        this.height = this.app.getContext().getSettings().getHeight();
        
        // Set first sky colors
        colorsHolder = new ColorsHolder();
        ColorsHolder.ColorsSet colors = colorsHolder.getRandomDayColors();
        topColor = colors.Color0.clone();
        bottomColor = colors.Color1.clone();
        
        
        // Load models
        loadModels();
    }

}
