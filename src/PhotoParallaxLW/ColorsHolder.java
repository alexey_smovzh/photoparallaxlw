/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package PhotoParallaxLW;

import com.jme3.math.Vector3f;
import com.jme3.math.FastMath;

/*
 * @author Alexey Smovzh <alexey.smovzh@gmail.com>
 *
 */

public class ColorsHolder {

    // holder for sky colors
    static class ColorsSet {
        
        Vector3f Color0;
        Vector3f Color1;
        
        public ColorsSet(Vector3f Color0, Vector3f Color1) {
            
            this.Color0 = Color0;
            this.Color1 = Color1;
            
        }        
    };
    
    private static final ColorsSet[] nightSkyColors = new ColorsSet[] {
        
            new ColorsSet(new Vector3f(.004f, .016f, .046f), new Vector3f(.005f, .102f, .334f)),
            new ColorsSet(new Vector3f(.006f, .008f, .014f), new Vector3f(.06f, .092f, .220f)),
            new ColorsSet(new Vector3f(.003f, .014f, .036f), new Vector3f(.027f, .097f, .22f)),
            new ColorsSet(new Vector3f(.011f, .011f, .012f), new Vector3f(.115f, .115f, .082f)),
            new ColorsSet(new Vector3f(0f, .005f, .003f), new Vector3f(.046f, .142f, .136f))
            
    };
    
    private static final ColorsSet[] daySkyColors = new ColorsSet[] {
                
            new ColorsSet(new Vector3f(0f, .027f, .127f), new Vector3f(.09f, .6f, 1f)),
            new ColorsSet(new Vector3f(.165f, .235f, .292f), new Vector3f(.533f, .708f, .799f)),            
            new ColorsSet(new Vector3f(0f, .161f, .246f), new Vector3f(.002f, .622f, 1f)),
            new ColorsSet(new Vector3f(.01f, .184f, .367f), new Vector3f(.434f, .584f, .8f))
            
    };
    
    private static final ColorsSet[] auroraColors = new ColorsSet[] {
                
            new ColorsSet(new Vector3f(0f, 1f, 0f), new Vector3f(0f, 0f, 1f)),
            new ColorsSet(new Vector3f(0f, 1f, 1f), new Vector3f(0f, 1f, 1f))
            
    };
    
    
    // Getters
    public ColorsSet getRandomDayColors() {
        
        int idx = FastMath.rand.nextInt(daySkyColors.length);
        
        return daySkyColors[idx];
        
    }
    
    public ColorsSet getRandomNightColors() {
        
        int idx = FastMath.rand.nextInt(nightSkyColors.length);        
        
        return nightSkyColors[idx];
        
    }
    
    public ColorsSet getRandomAuroraColors() {
        
        int idx = FastMath.rand.nextInt(auroraColors.length);        
        
        return auroraColors[idx];
        
    }
}
