/*
* fragment shader template
*/
#ifdef GL_ES
precision mediump float;
#endif

uniform sampler2D m_ColorMap;
uniform vec2 m_Resolution;

varying vec2 texCoord;
varying float time;

uniform vec3 m_Color0;  
uniform vec3 m_Color1;  


vec3 c(float d) {	
    return (m_Color0 * (0.7 - sin(d*3.0+time) / m_Resolution.x)) + (m_Color1 * sin(d*3.0+time));	
}	

void main() {

    vec4 texColor = texture2D(m_ColorMap, texCoord);
    vec2 position = ( gl_FragCoord.xy / m_Resolution.xy ); 

    gl_FragColor = vec4(texColor.rgb*c(position.x), 1.0);
      
}

